package com.example.eruca.ui.sensorsData;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.MainActivity;
import com.example.eruca.R;
import com.example.eruca.helpers.Box;
import com.example.eruca.helpers.SharedViewModel;
import com.example.eruca.ui.sensorsData.manual.ManualBrightnessFragment;
import com.example.eruca.ui.sensorsData.manual.ManualPHFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("UseSwitchCompatOrMaterialCode")
public class SensorsDataFragment extends Fragment {

    //init view model class for getting data from GetBoxesFragment
    private SharedViewModel sharedViewModel;

    //init queue for GET and POST requests
    private RequestQueue mQueue;

    //init text for card's values
    private TextView mAirTemperatureValue;
    private TextView mAirHumidityValue;
    private TextView mPHValue;
    private TextView mESValue;
    private TextView mAllFluidLevelValue;

    //init texts for box's values
    private TextView mBoxIdValue;
    private TextView mBoxTitleValue;

    //init list views for Translucency and FluidLevel cards
    private ListView mListTranslucency;
    private ListView mListFluidLevel;

    //init switches for cards
    private Switch mAirTemperatureSwitch;
    private Switch mWaterLevelSwitch;
    private Switch mMainWaterLevelSwitch;
    private Switch mHumiditySwitch;
    private Switch mBrightnessSwitch;
    private Switch mPHSwitch;
    private Switch mEsSwitch;

    //init auth token
    private String TOKEN;

    //init strings for box values
    private String BOX_ID = "";
    private String BOX_TITLE = "";

    private final Context context = getContext();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        Observer<Box> observer = new Observer<Box>() {
            @Override
            public void onChanged(Box box) {
                BOX_ID = box.getBoxId();
                BOX_TITLE = box.getBoxTitle();

                jsonParse(BOX_ID);

                mBoxIdValue.setText(getString(R.string.box_id, BOX_ID));
                mBoxTitleValue.setText(getString(R.string.box_title, BOX_TITLE));
            }
        };

        sharedViewModel.getData().observe(this, observer);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_sensors_data, container, false);

        return root;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //init queue for get/post requests
        mQueue = Volley.newRequestQueue(getContext());

        MainActivity activity = (MainActivity) getActivity();

        //get auth token from parent activity
        TOKEN = activity.getToken();

        //init buttons for manual control in different cards
        ImageView mManualAirControl = view.findViewById(R.id.manual_control_air_temp);
        ImageView mManualWaterControl = view.findViewById(R.id.manual_control_water_temp);
        ImageView mManualHumidityControl = view.findViewById(R.id.manual_control_humidity);
        ImageView mMainManualWaterControl = view.findViewById(R.id.manual_control_main_water_temp);
        ImageView mManualBrightnessControl = view.findViewById(R.id.manual_control_brightness);
        ImageView mManualPHControl = view.findViewById(R.id.manual_control_ph);
        ImageView mManualESControl = view.findViewById(R.id.manual_control_es);

        //init text views for setting values from JSON
        mAirTemperatureValue = view.findViewById(R.id.air_temperature_value);
        mAirHumidityValue = view.findViewById(R.id.humidity_value);
        mPHValue = view.findViewById(R.id.ph_value);
        mESValue = view.findViewById(R.id.es_value);
        mAllFluidLevelValue = view.findViewById(R.id.fluid_level_value);

        //init text views for box parameters
        mBoxIdValue = view.findViewById(R.id.id_box_monitor);
        mBoxTitleValue = view.findViewById(R.id.title_box_monitor);

        //init lists for translucency and fluid level cards
        mListTranslucency = view.findViewById(R.id.translucency_list);
        mListFluidLevel = view.findViewById(R.id.fluid_level_list);

        //init switches to manual control
        mAirTemperatureSwitch = view.findViewById(R.id.air_temperature_switch);
        mWaterLevelSwitch = view.findViewById(R.id.water_temperature_switch);
        mMainWaterLevelSwitch = view.findViewById(R.id.main_water_temperature_switch);
        mHumiditySwitch = view.findViewById(R.id.humidity_switch);
        mBrightnessSwitch = view.findViewById(R.id.brightness_switch);
        mPHSwitch = view.findViewById(R.id.ph_switch);
        mEsSwitch = view.findViewById(R.id.es_switch);

        //using private class for switches control
        mAirTemperatureSwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mAirTemperatureSwitch, mManualAirControl));
        mWaterLevelSwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mWaterLevelSwitch, mManualWaterControl));
        mHumiditySwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mHumiditySwitch, mManualHumidityControl));
        mBrightnessSwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mBrightnessSwitch, mManualBrightnessControl));
        mMainWaterLevelSwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mMainWaterLevelSwitch, mMainManualWaterControl));
        mPHSwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mPHSwitch, mManualPHControl));
        mEsSwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mEsSwitch, mManualESControl));

        //using private class for opening new fragments
        mManualAirControl.setOnClickListener(new SetClickListener(context, mManualAirControl, 0, "ManualTemperatureFragment"));
        mManualWaterControl.setOnClickListener(new SetClickListener(context, mManualWaterControl, 1, "ManualWaterFragment"));
        mManualHumidityControl.setOnClickListener(new SetClickListener(context, mManualHumidityControl, 2, "ManualHumidityFragment"));
        mMainManualWaterControl.setOnClickListener(new SetClickListener(context, mMainManualWaterControl, 3, "ManualMainWaterFragment"));
        mManualBrightnessControl.setOnClickListener(new SetClickListener(context, mManualBrightnessControl, new ManualBrightnessFragment(), "ManualBrightnessFragment"));
        mManualPHControl.setOnClickListener(new SetClickListener(context, mManualPHControl, new ManualPHFragment(), "ManualPHFragment"));
        mManualESControl.setOnClickListener(new SetClickListener(context, mManualESControl, new ManualPHFragment(), "ManualEsFragment"));
    }

    private void jsonParse(String id) {
        String url = BuildConfig.SERVER_URL + "/api/sensors/" + id;
        Log.e("JSON_ID: ", id);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                response -> {
                    try {
                        JSONArray jsonArray = response.getJSONArray("SensorDatas");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject sensorsData = jsonArray.getJSONObject(i);

                            double humidity = sensorsData.getDouble("humidity");
                            double temperature = sensorsData.getDouble("temperature");

                            double translucency1 = sensorsData.getDouble("translucency1");
                            double translucency2 = sensorsData.getDouble("translucency2");
                            double translucency3 = sensorsData.getDouble("translucency3");

                            double fluidLevel1 = sensorsData.getDouble("fluidLevel1");
                            double fluidLevel2 = sensorsData.getDouble("fluidLevel2");
                            double fluidLevel3 = sensorsData.getDouble("fluidLevel3");

                            double mainFluidLevel = sensorsData.getDouble("mainFluidLevel");
                            double ph = sensorsData.getDouble("ph");
                            double es = sensorsData.getDouble("es");

                            createSensorsDataList(String.valueOf(translucency1), String.valueOf(translucency2),
                                    String.valueOf(translucency3), " lux", mListTranslucency);
                            createSensorsDataList(String.valueOf(fluidLevel1), String.valueOf(fluidLevel2),
                                    String.valueOf(fluidLevel3), " %", mListFluidLevel);

                            String temperatureString = getString(R.string.air_temp_value, temperature);
                            String humidityString = getString(R.string.air_humidity_value, humidity);
                            String phString = getString(R.string.ph_value, ph);
                            String esString = getString(R.string.es_value, es);
                            String allFluidLevelString = getString(R.string.fluid_level, mainFluidLevel);

                            mAirTemperatureValue.setText(temperatureString);
                            mAirHumidityValue.setText(humidityString);
                            mPHValue.setText(phString);
                            mESValue.setText(esString);
                            mAllFluidLevelValue.setText(allFluidLevelString);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + TOKEN);

                return headerMap;
            }
        };

        mQueue.add(request);
    }

    private void createSensorsDataList(String arg1, String arg2, String arg3, String unit, ListView listView) {
        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("1. " + arg1 + unit);
        arrayList.add("2. " + arg2 + unit);
        arrayList.add("3. " + arg3 + unit);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(adapter);
    }

    private double convertValues(double value, int maxValue) {
        return ((double) value * 100) / (double) maxValue;
    }

    private int convertValues(int value, int maxValue) {
        return (value * 100) / maxValue;
    }

    private class SetSwitchListener implements CompoundButton.OnCheckedChangeListener {
        private Context mContext;
        Switch mSwitch;
        ImageView mImageView;

        public SetSwitchListener(Context mContext, Switch mSwitch, ImageView mImageView) {
            this.mContext = mContext;
            this.mSwitch = mSwitch;
            this.mImageView = mImageView;
        }


        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (b) {
                mImageView.setVisibility(View.VISIBLE);
            } else {
                mImageView.setVisibility(View.INVISIBLE);
            }
        }
    }

    private class SetClickListener implements View.OnClickListener {
        private Context mContext;
        ImageView mImageView;
        Fragment mFragment;
        String tag;
        int position;

        public SetClickListener(Context mContext, ImageView mImageView, Fragment mFragment, String tag) {
            this.mContext = mContext;
            this.mImageView = mImageView;
            this.mFragment = mFragment;
            this.tag = tag;
        }

        public SetClickListener(Context mContext, ImageView mImageView, int position, String tag) {
            this.mContext = mContext;
            this.mImageView = mImageView;
            this.position = position;
            this.tag = tag;
        }

        @Override
        public void onClick(View view) {

        }
    }
}