package com.example.eruca.ui.qr;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.example.eruca.BuildConfig;
import com.example.eruca.MainActivity;
import com.example.eruca.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class QrScannerFragment extends Fragment {

    //init camera constant to ask user about camera permission in this app
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 123;

    //init queue for post/get requests
    private RequestQueue mQueue;

    //init scanner for QR-codes
    private CodeScanner mCodeScanner;

    //init string for auth token
    String token;

    View view;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_qr_scanner, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mQueue = Volley.newRequestQueue(getContext());
        MainActivity activity = (MainActivity) getActivity();
        token = activity.getToken();
        this.view = view;

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[] {Manifest.permission.CAMERA}, REQUEST_PERMISSIONS_REQUEST_CODE);
        } else {
            scanQR(view);
        }
    }

    private void jsonSend(String boxName) {
        String url = BuildConfig.SERVER_URL + "/api/box/";
        JSONObject postData = new JSONObject();

        try {
            postData.put("qrHash", boxName);
            postData.put("model", "V.0.0.2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postData, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Success", postData.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("JSON Object", postData.toString());
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + token);

                return headerMap;
            }
        };

        mQueue.add(jsonObjectRequest);
    }

    public void scanQR(View view) {
        final Activity activity = getActivity();

        CodeScannerView scannerView = view.findViewById(R.id.qr_code_scanner);
        mCodeScanner = new CodeScanner(activity, scannerView);

        mCodeScanner.setDecodeCallback(result -> activity.runOnUiThread(() -> {
            //Toast.makeText(activity, result.getText(), Toast.LENGTH_SHORT).show();
            String qrText = result.getText();
            jsonSend(qrText);
        }));
        scannerView.setOnClickListener(view1 -> mCodeScanner.startPreview());
    }

    @Override
    public void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    public void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Camera permission granted", Toast.LENGTH_SHORT).show();
                scanQR(view);
            } else {
                Toast.makeText(getContext(), "Camera permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }
}