package com.example.eruca.ui.technicalCard;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.MainActivity;
import com.example.eruca.R;
import com.example.eruca.helpers.Box;
import com.example.eruca.helpers.SharedViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TechnicalCardFragment extends Fragment {

    //init view model class for getting data from GetBoxesFragment
    private SharedViewModel sharedViewModel;

    //init queue for post/get requests
    private RequestQueue mPOSTQueue;
    private RequestQueue mGETQueue;

    //init text fields to sending data
    private EditText mDay;
    private EditText mNight;
    private EditText mBeginWeek;
    private EditText mEndWeek;
    private EditText mTempMin;
    private EditText mTempMax;
    private EditText mHumMin;
    private EditText mHumMax;
    private EditText mTransMin;
    private EditText mTransMax;
    private EditText mFluidMin;
    private EditText mFluidMax;
    private EditText mMainFluidMin;
    private EditText mMainFluidMax;
    private EditText mPhMin;
    private EditText mPhMax;
    private EditText mEsMin;
    private EditText mEsMax;

    //init texts for box's values
    private TextView mBoxIdValue;
    private TextView mBoxTitleValue;

    //init button to confirm sending
    private Button mButton;

    //init string to getting authorization token
    private String TOKEN;

    //init variable for box id
    private String BOX_ID;
    private String BOX_TITLE;

    //init variable to getting activity context
    private final Context context = getContext();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        Observer<Box> observer = new Observer<Box>() {
            @Override
            public void onChanged(Box box) {
                BOX_ID = box.getBoxId();
                BOX_TITLE = box.getBoxTitle();

                jsonParse(BOX_ID);

                mBoxIdValue.setText(getString(R.string.box_id, BOX_ID));
                mBoxTitleValue.setText(getString(R.string.box_title, BOX_TITLE));
            }
        };

        sharedViewModel.getData().observe(this, observer);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_technical_card, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //init queues for post and get requests
        mPOSTQueue = Volley.newRequestQueue(getContext());
        mGETQueue = Volley.newRequestQueue(getContext());

        //get activity for getting token
        MainActivity activity = (MainActivity) getActivity();

        //get token from parent activity
        TOKEN = activity.getToken();

        //init text views for box parameters
        mBoxIdValue = view.findViewById(R.id.id_box_tech_card);
        mBoxTitleValue = view.findViewById(R.id.title_box_tech_card);

        //find views
        mDay = view.findViewById(R.id.input_sun);
        mNight = view.findViewById(R.id.input_moon);
        mBeginWeek = view.findViewById(R.id.input_begin_week);
        mEndWeek = view.findViewById(R.id.input_end_week);
        mTempMin = view.findViewById(R.id.min_temp);
        mTempMax = view.findViewById(R.id.max_temp);
        mHumMin = view.findViewById(R.id.min_hum);
        mHumMax = view.findViewById(R.id.max_hum);
        mTransMin = view.findViewById(R.id.min_trans);
        mTransMax = view.findViewById(R.id.max_trans);
        mFluidMin = view.findViewById(R.id.min_fluid);
        mFluidMax = view.findViewById(R.id.max_fluid);
        mMainFluidMin = view.findViewById(R.id.min_main_fluid);
        mMainFluidMax = view.findViewById(R.id.max_main_fluid);
        mPhMin = view.findViewById(R.id.min_ph);
        mPhMax = view.findViewById(R.id.max_ph);
        mEsMin = view.findViewById(R.id.min_es);
        mEsMax = view.findViewById(R.id.max_es);

        //find button
        mButton = view.findViewById(R.id.create_card_btn);

        //use private class to saving data from fields
        mDay.addTextChangedListener(new AddListenerOnTextChange(context, mDay, "dayBegin"));
        mNight.addTextChangedListener(new AddListenerOnTextChange(context, mNight, "nightBegin"));

        mBeginWeek.addTextChangedListener(new AddListenerOnTextChange(context, mBeginWeek, "beginWeek"));
        mEndWeek.addTextChangedListener(new AddListenerOnTextChange(context, mEndWeek, "endWeek"));

        mTempMin.addTextChangedListener(new AddListenerOnTextChange(context, mTempMin, "tempMin"));
        mTempMax.addTextChangedListener(new AddListenerOnTextChange(context, mTempMax, "tempMax"));

        mHumMin.addTextChangedListener(new AddListenerOnTextChange(context, mHumMin, "humMin"));
        mHumMax.addTextChangedListener(new AddListenerOnTextChange(context, mHumMax, "humMax"));

        mTransMin.addTextChangedListener(new AddListenerOnTextChange(context, mTransMin, "transMin"));
        mTransMax.addTextChangedListener(new AddListenerOnTextChange(context, mTransMax, "transMax"));

        mFluidMin.addTextChangedListener(new AddListenerOnTextChange(context, mFluidMin, "fluidMin"));
        mFluidMax.addTextChangedListener(new AddListenerOnTextChange(context, mFluidMax, "fluidMax"));

        mMainFluidMin.addTextChangedListener(new AddListenerOnTextChange(context, mMainFluidMin, "mainFluidMin"));
        mMainFluidMax.addTextChangedListener(new AddListenerOnTextChange(context, mMainFluidMax, "mainFluidMax"));

        mPhMin.addTextChangedListener(new AddListenerOnTextChange(context, mPhMin, "phMin"));
        mPhMax.addTextChangedListener(new AddListenerOnTextChange(context, mPhMax, "phMax"));

        mEsMin.addTextChangedListener(new AddListenerOnTextChange(context, mEsMin, "esMin"));
        mEsMax.addTextChangedListener(new AddListenerOnTextChange(context, mEsMax, "esMax"));

        //send data to server
        mButton.setOnClickListener(view1 -> jsonSend(BOX_ID));

        //check data in input fields and change button state
        if (checkData()) {
            mButton.setAlpha(1);
            mButton.setClickable(true);
        } else {
            mButton.setAlpha(0.5F);
            mButton.setClickable(false);
        }
    }

    private void jsonParse(String id) {
        String url = BuildConfig.SERVER_URL + "/api/technicalCard/" + id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            try {
                JSONArray jsonArray = response.getJSONArray("TechnicalCards");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject techCard = jsonArray.getJSONObject(i);
                    JSONArray weeksArray = techCard.getJSONArray("weeks");
                    JSONArray dayLightArray = techCard.getJSONArray("daylight");
                    JSONArray tempArray = techCard.getJSONArray("temperature");
                    JSONArray humArray = techCard.getJSONArray("humidity");
                    JSONArray transArray = techCard.getJSONArray("translucency");
                    JSONArray fluidArray = techCard.getJSONArray("fluidLevel");
                    JSONArray mainFluidArray = techCard.getJSONArray("mainFluidLevel");
                    JSONArray phArray = techCard.getJSONArray("ph");
                    JSONArray esArray = techCard.getJSONArray("es");

                    //init arrays to prepare technical card for getting data from server
                    int[] weeks = new int[2];
                    int[] dayLight = new int[2];
                    double[] temp = new double[2];
                    double[] hum = new double[2];
                    double[] trans = new double[2];
                    double[] fluid = new double[2];
                    double[] mainFluid = new double[2];
                    double[] ph = new double[2];
                    double[] es = new double[2];

                    //set values in arrays
                    for (int j = 0; j < 2; j++) {
                        weeks[j] = weeksArray.getInt(j);
                        dayLight[j] = dayLightArray.getInt(j);
                        temp[j] = tempArray.getDouble(j);
                        hum[j] = humArray.getDouble(j);
                        trans[j] = transArray.getDouble(j);
                        fluid[j] = fluidArray.getDouble(j);
                        mainFluid[j] = mainFluidArray.getDouble(j);
                        ph[j] = phArray.getDouble(j);
                        es[j] = esArray.getDouble(j);
                    }

                    //set values from arrays to text fields
                    mBeginWeek.setText(String.valueOf(weeks[0]));
                    mEndWeek.setText(String.valueOf(weeks[1]));

                    mDay.setText(String.valueOf(dayLight[0]));
                    mNight.setText(String.valueOf(dayLight[1]));

                    mTempMin.setText(String.valueOf(temp[0]));
                    mTempMax.setText(String.valueOf(temp[1]));

                    mHumMin.setText(String.valueOf(hum[0]));
                    mHumMax.setText(String.valueOf(hum[1]));

                    mTransMin.setText(String.valueOf(trans[0]));
                    mTransMax.setText(String.valueOf(trans[1]));

                    mFluidMin.setText(String.valueOf(fluid[0]));
                    mFluidMax.setText(String.valueOf(fluid[1]));

                    mMainFluidMin.setText(String.valueOf(mainFluid[0]));
                    mMainFluidMax.setText(String.valueOf(mainFluid[1]));

                    mPhMin.setText(String.valueOf(ph[0]));
                    mPhMax.setText(String.valueOf(ph[1]));

                    mEsMin.setText(String.valueOf(es[0]));
                    mEsMax.setText(String.valueOf(es[1]));

//                    for (int j = 0; j < editTexts.length; j++) {
//                        editTexts[j].setTextColor(Color.GRAY);
//                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace) {

            //create header for authorization
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + TOKEN);

                return headerMap;
            }
        };

        mGETQueue.add(jsonObjectRequest);
    }

    private void jsonSend(String id) {
        //String url = "https://webhook.site/5e550c39-72e9-468f-ae8d-1f8727e9d5f5";
        String url = BuildConfig.SERVER_URL + "/api/technicalCard/";
        JSONObject postData = new JSONObject();

        //init arrays to prepare them for json
        int[] weeks = {readIntDataFromFile("beginWeek"), readIntDataFromFile("endWeek")};
        int[] lightAmount = {readIntDataFromFile("dayBegin"), readIntDataFromFile("nightBegin")};
        int[] fertilizer = {0, 1};
        float[] temperature = {readFloatDataFromFile("tempMin"), readFloatDataFromFile("tempMax")};
        float[] humidity = {readFloatDataFromFile("humMin"), readFloatDataFromFile("humMax")};
        float[] translucency = {readFloatDataFromFile("transMin"), readFloatDataFromFile("transMax")};
        float[] fluidLevel = {readFloatDataFromFile("fluidMin"), readFloatDataFromFile("fluidMax")};
        float[] mainFluidLevel = {readFloatDataFromFile("mainFluidMin"), readFloatDataFromFile("mainFluidMax")};
        float[] raindrop = {0.1F, 2.3F};
        float[] ph = {readFloatDataFromFile("phMin"), readFloatDataFromFile("phMax")};
        float[] es = {readFloatDataFromFile("esMin"), readFloatDataFromFile("esMax")};

        //put data in json object for technical card
        try {
            postData.put("boxId", id);
            postData.put("weeks", new JSONArray(weeks));
            postData.put("daylight", new JSONArray(lightAmount));
            postData.put("fertilizer", new JSONArray(fertilizer));
            postData.put("temperature", new JSONArray(temperature));
            postData.put("humidity", new JSONArray(humidity));
            postData.put("translucency", new JSONArray(translucency));
            postData.put("fluidLevel", new JSONArray(fluidLevel));
            postData.put("mainFluidLevel", new JSONArray(mainFluidLevel));
            postData.put("raindrop", new JSONArray(raindrop));
            postData.put("ph", new JSONArray(ph));
            postData.put("es", new JSONArray(es));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //prepare whole json object for sending to server
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postData,
                response -> Log.e("Success: ", postData.toString()),
                Throwable::printStackTrace) {

            //create header for authorization
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + TOKEN);

                return headerMap;
            }
        };

        mPOSTQueue.add(jsonObjectRequest);
    }

    private int parseIntFromString(String str) {
        int res;
        try {
            res = Integer.parseInt(str);

            return res;
        } catch (NumberFormatException e) {
            res = 0;

            return res;
        }
    }

    private float parseFloatFromString(String str) {
        float res;
        try {
            res = Float.parseFloat(str);

            return res;
        } catch (NumberFormatException e) {
            res = 0;

            return res;
        }
    }

    private int readIntDataFromFile(String key) {
        SharedPreferences mPreferences;

        mPreferences = getContext().getSharedPreferences("techCard", Context.MODE_PRIVATE);
        String res = mPreferences.getString(key, "");

        return parseIntFromString(res);
    }

    private float readFloatDataFromFile(String key) {
        SharedPreferences mPreferences;

        mPreferences = getContext().getSharedPreferences("techCard", Context.MODE_PRIVATE);
        String res = mPreferences.getString(key, "");

        return parseFloatFromString(res);
    }

    private boolean checkData() {
        SharedPreferences mPreferences;

        mPreferences = getContext().getSharedPreferences("techCard", Context.MODE_PRIVATE);

        if (mPreferences.getString("dayBegin", "").equals("") ||
                mPreferences.getString("nightBegin", "").equals("") ||
                mPreferences.getString("phMin", "").equals("") ||
                mPreferences.getString("phMax", "").equals("") ||
                mPreferences.getString("esMin", "").equals("") ||
                mPreferences.getString("esMax", "").equals("") ||
                mPreferences.getString("beginWeek", "").equals("") ||
                mPreferences.getString("endWeek", "").equals("") ||
                mPreferences.getString("tempMin", "").equals("") ||
                mPreferences.getString("tempMax", "").equals("") ||
                mPreferences.getString("humMin", "").equals("") ||
                mPreferences.getString("humMax", "").equals("") ||
                mPreferences.getString("transMin", "").equals("") ||
                mPreferences.getString("transMax", "").equals("") ||
                mPreferences.getString("fluidMin", "").equals("") ||
                mPreferences.getString("fluidMax", "").equals("") ||
                mPreferences.getString("mainFluidMin", "").equals("") ||
                mPreferences.getString("mainFluidMax", "").equals("")) {

            return false;
        } else {
            return true;
        }
    }

    private void makeToastMessage(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    private class AddListenerOnTextChange implements TextWatcher {

        private Context mContext;
        EditText mEditText;

        private SharedPreferences mPreferences;
        private SharedPreferences.Editor mEditor;

        private String key;

        public AddListenerOnTextChange(Context mContext, EditText mEditText, String key) {
            this.mContext = mContext;
            this.mEditText = mEditText;
            this.key = key;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (checkData()) {
                mButton.setAlpha(1);
                mButton.setClickable(true);
            } else {
                mButton.setAlpha(0.5F);
                mButton.setClickable(false);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int weeksBeginAmount = parseIntFromString(String.valueOf(mEditText.getText()));
            int weeksEndAmount = parseIntFromString(String.valueOf(mEditText.getText()));
            int dayBeginAmount = parseIntFromString(String.valueOf(mEditText.getText()));
            int dayEndAmount = parseIntFromString(String.valueOf(mEditText.getText()));
            float phMinAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float phMaxAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float esMinAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float esMaxAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float tempMinAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float tempMaxAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float humMinAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float humMaxAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float transMinAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float transMaxAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float fluidMinAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float fluidMaxAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float mainFluidMinAmount = parseFloatFromString(String.valueOf(mEditText.getText()));
            float mainFluidMaxAmount = parseFloatFromString(String.valueOf(mEditText.getText()));

            switch (mEditText.getId()) {
                case R.id.input_sun:
                    changeButtonState(dayBeginAmount, 25, s, "You can put number only between 0 and 24!");
                    break;

                case R.id.input_moon:
                    changeButtonState(dayEndAmount, 25, s, "You can put number only between 0 and 24!");
                    break;

                case R.id.input_begin_week:
                    changeButtonState(weeksBeginAmount, 5, s, "You can put number only between 0 and 4!");
                    break;

                case R.id.input_end_week:
                    changeButtonState(weeksEndAmount, 5, s, "You can put number only between 0 and 4!");
                    break;

                case R.id.min_temp:
                    changeButtonState(tempMinAmount, 80, s, "You can put number only between 0 and 80!");
                    break;

                case R.id.max_temp:
                    changeButtonState(tempMaxAmount, 80, s, "You can put number only between 0 and 80!");
                    break;

                case R.id.min_hum:
                    changeButtonState(humMinAmount, 101, s, "You can put number only between 0 and 100!");
                    break;

                case R.id.max_hum:
                    changeButtonState(humMaxAmount, 101, s, "You can put number only between 0 and 100!");
                    break;

                case R.id.min_trans:
                    changeButtonState(transMinAmount, 2001, s, "You can put number only between 0 and 2000!");
                    break;

                case R.id.max_trans:
                    changeButtonState(transMaxAmount, 2001, s, "You can put number only between 0 and 2000!");
                    break;

                case R.id.min_fluid:
                    changeButtonState(fluidMinAmount, 101, s, "You can put number only between 0 and 100!");
                    break;

                case R.id.max_fluid:
                    changeButtonState(fluidMaxAmount, 101, s, "You can put number only between 0 and 100!");
                    break;

                case R.id.min_main_fluid:
                    changeButtonState(mainFluidMinAmount, 101, s, "You can put number only between 0 and 100!");
                    break;

                case R.id.max_main_fluid:
                    changeButtonState(mainFluidMaxAmount, 101, s, "You can put number only between 0 and 100!");
                    break;

                case R.id.min_ph:
                    changeButtonState(phMinAmount, 15, s, "You can put number only between 0 and 14!");
                    break;

                case R.id.max_ph:
                    changeButtonState(phMaxAmount, 15, s, "You can put number only between 0 and 14!");
                    break;

                case R.id.min_es:
                    changeButtonState(esMinAmount, 6, s, "You can put number only between 0 and 5!");
                    break;

                case R.id.max_es:
                    changeButtonState(esMaxAmount, 6, s, "You can put number only between 0 and 5!");
                    break;
            }
        }

        private void saveDataFromFields(String key, String value) {
            mPreferences = getActivity().getSharedPreferences("techCard", Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();

            mEditor.putString(key, value);
            mEditor.apply();
        }

        private void changeButtonState(float field, int parameter, CharSequence s, String msg) {
            if (field > -1 && field < parameter) {
                saveDataFromFields(this.key, s.toString());

            } else {
                makeToastMessage(msg);
                mButton.setAlpha(0.5F);
                mButton.setClickable(false);
            }
        }
    }
}
