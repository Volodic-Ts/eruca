package com.example.eruca.ui.modulesState;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.BuildConfig;
import com.example.eruca.MainActivity;
import com.example.eruca.R;
import com.example.eruca.helpers.Box;
import com.example.eruca.helpers.SharedViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@SuppressLint("UseSwitchCompatOrMaterialCode")
public class ModulesStateFragment extends Fragment {

    //init view model class for getting data from GetBoxesFragment
    private SharedViewModel sharedViewModel;

    //init queue for post/get requests
    private RequestQueue mQueue;

    //init switch components
    private Switch mPHSwitch;
    private Switch mLightSwitch;
    private  Switch mESSwitch;

    //init images for icons
    private ImageView mPH;
    private ImageView mLight;
    private ImageView mES;

    //init texts for box's values
    private TextView mBoxIdValue;
    private TextView mBoxTitleValue;

    //init strings for getting box values and token
    private String BOX_ID;
    private String BOX_TITLE;
    private String TOKEN;

    private final Context context = getContext();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        Observer<Box> observer = new Observer<Box>() {
            @Override
            public void onChanged(Box box) {
                BOX_ID = box.getBoxId();
                BOX_TITLE = box.getBoxTitle();

                mBoxIdValue.setText(getString(R.string.box_id, BOX_ID));
                mBoxTitleValue.setText(getString(R.string.box_title, BOX_TITLE));
            }
        };

        sharedViewModel.getData().observe(this, observer);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_modules_state, container, false);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MainActivity activity = (MainActivity) getActivity();

        //create queue for post/get requests
        mQueue = Volley.newRequestQueue(getContext());

        TOKEN = activity.getToken();

        //init text views for box parameters
        mBoxIdValue = view.findViewById(R.id.id_box_modules);
        mBoxTitleValue = view.findViewById(R.id.title_box_modules);

        //init switches
        mPHSwitch = view.findViewById(R.id.ph_module_switch);
        mLightSwitch = view.findViewById(R.id.light_switch);
        mESSwitch = view.findViewById(R.id.es_module_switch);

        //init icons
        mLight = view.findViewById(R.id.ic_light);
        mPH = view.findViewById(R.id.ic_ph);
        mES = view.findViewById(R.id.ic_es);

        //use private class for switches control
        mLightSwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mLightSwitch, mLight, R.drawable.ic_light_on, R.drawable.ic_light_off));
        mPHSwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mPHSwitch, mPH, R.drawable.ic_humidity_high_on, R.drawable.ic_humidity_high_off));
        mESSwitch.setOnCheckedChangeListener(new SetSwitchListener(context, mESSwitch, mES, R.drawable.ic_humidity_low_on, R.drawable.ic_humidity_low_off));
    }

    private void jsonSend(String id) {
        //Log.e("STATE_ID: ", id);
        String url = BuildConfig.SERVER_URL + "/api/modules/";
        JSONObject postData = new JSONObject();

        try {
            postData.put("boxId", id);
            postData.put("light", mLightSwitch.isChecked());
            postData.put("ph", mPHSwitch.isChecked());
            postData.put("es", mESSwitch.isChecked());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postData,
                response -> Log.e("Success", postData.toString()),
                Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + TOKEN);

                return headerMap;
            }
        };

        mQueue.add(jsonObjectRequest);
    }

    private class SetSwitchListener implements CompoundButton.OnCheckedChangeListener {
        private Context mContext;
        Switch mSwitch;
        ImageView mImageView;
        int imageKey, alternateImageKey;

        public SetSwitchListener(Context mContext, Switch mSwitch, ImageView mImageView, int imageKey, int alternateImageKey) {
            this.mContext = mContext;
            this.mSwitch = mSwitch;
            this.mImageView = mImageView;
            this.imageKey = imageKey;
            this.alternateImageKey = alternateImageKey;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            jsonSend(BOX_ID);

            if (b) {
                mImageView.setImageResource(imageKey);
            } else {
                mImageView.setImageResource(alternateImageKey);
            }
        }
    }
}