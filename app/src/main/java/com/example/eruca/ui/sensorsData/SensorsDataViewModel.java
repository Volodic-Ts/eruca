package com.example.eruca.ui.sensorsData;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SensorsDataViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SensorsDataViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("All data in one place");
    }

    public LiveData<String> getText() {
        return mText;
    }
}