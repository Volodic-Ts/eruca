package com.example.eruca.onBoarding;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.eruca.AuthorizationActivity;
import com.example.eruca.R;

public class Splash extends AppCompatActivity {

    //init component
    LauncherManager launcherManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();

        launcherManager = new LauncherManager(this);

        new Handler().postDelayed(() -> {
            if(launcherManager.isFirstTime()) {
                launcherManager.setFirstLaunch(false);
                startActivity(new Intent(getApplicationContext(), Slider.class));
            } else {
                startActivity(new Intent(getApplicationContext(), AuthorizationActivity.class));
            }
        }, 1000);
    }
}
