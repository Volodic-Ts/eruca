package com.example.eruca.onBoarding;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.eruca.AuthorizationActivity;
import com.example.eruca.R;

public class Slider extends AppCompatActivity {

    //init components
    ViewPager viewPager;
    Button nextButton;
    int[] layouts;
    Adapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_slider);

        getSupportActionBar().hide();

        //find views
        viewPager = findViewById(R.id.view_pager);
        nextButton = findViewById(R.id.next_button);

        //add slider components in array
        layouts = new int[]{
                R.layout.slider1,
                R.layout.slider2,
                R.layout.slider3
        };

        //init adapter
        adapter = new Adapter(this, layouts);
        viewPager.setAdapter(adapter);

        nextButton.setOnClickListener(v -> {
            if (viewPager.getCurrentItem() + 1 < layouts.length) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            } else {
                startActivity(new Intent(getApplicationContext(), AuthorizationActivity.class));
            }
        });

        viewPager.addOnPageChangeListener(viewPagerChangeListener);
    }

    ViewPager.OnPageChangeListener viewPagerChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (position == layouts.length - 1) {
                nextButton.setText(R.string.slides_button_continue);
            } else {
                nextButton.setText(R.string.slides_button_next);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}
