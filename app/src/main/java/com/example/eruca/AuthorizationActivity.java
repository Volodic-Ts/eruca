package com.example.eruca;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class AuthorizationActivity extends AppCompatActivity {

    //init queue variable for post/get requests
    private RequestQueue mQueue;

    //init activity components
    private Button mSignInButton;
    private EditText mLoginText;
    private EditText mPasswordText;
    private CheckBox mRememberCheckBox;

    //init variables for saving auth data in file
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    //init string for getting auth token
    private String token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        getSupportActionBar().hide();

        mQueue = Volley.newRequestQueue(this);

        mLoginText = findViewById(R.id.login_input);
        mPasswordText = findViewById(R.id.password_input);
        mSignInButton = findViewById(R.id.sign_in_button);
        mRememberCheckBox = findViewById(R.id.remember_checkBox);

        //load data form shared preferences
        mPreferences = getSharedPreferences("saveLogin", MODE_PRIVATE);
        String checkbox = mPreferences.getString("remember", "");

        //check about remembering user
        if (checkbox.equals("true")) {
            jsonSend(mPreferences.getString("login", ""), mPreferences.getString("password", ""));
        } else {
            mSignInButton.setOnClickListener(view ->
                    jsonSend(mLoginText.getText().toString(), mPasswordText.getText().toString()));
        }

        mRememberCheckBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (compoundButton.isChecked() && mLoginText.getText().length() > 0 && mPasswordText.getText().length() > 0) {
                mPreferences = getSharedPreferences("saveLogin", MODE_PRIVATE);
                mEditor = mPreferences.edit();
                mEditor.putString("login", mLoginText.getText().toString());
                mEditor.putString("password", mPasswordText.getText().toString());
                mEditor.putString("remember", "true");
                mEditor.apply();
                Toast.makeText(AuthorizationActivity.this, "Checked!", Toast.LENGTH_SHORT).show();
            } else if (!compoundButton.isChecked()) {
                mPreferences = getSharedPreferences("saveLogin", MODE_PRIVATE);
                mEditor = mPreferences.edit();
                mEditor.putString("remember", "false");
                mEditor.apply();
                Toast.makeText(AuthorizationActivity.this, "Unchecked!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void jsonSend(String log, String pass) {
        String url = BuildConfig.SERVER_URL + "/auth/sign-in";
        JSONObject postData = new JSONObject();

        try {
            postData.put("username", log);
            postData.put("password", pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, postData, response -> {

            mPreferences = getSharedPreferences("tokenPrefs", Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();

            try {
                token = response.getString("token");

                mEditor.putString("token", token);
                mEditor.commit();

                Log.e("Success: ", postData.toString());
                Log.e("Success: ", token);

                startActivity(new Intent(getApplicationContext(), MainActivity.class));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> Log.e("Error: ", postData.toString()));

        mQueue.add(jsonObjectRequest);
    }
}
