package com.example.eruca.helpers;

//public class for passing box values between fragments via ViewModel
public class Box {
    private String boxId;
    private String boxTitle;

    public Box(String boxId, String boxTitle) {
        this.boxId = boxId;
        this.boxTitle = boxTitle;
    }

    public String getBoxId() {
        return boxId;
    }

    public String getBoxTitle() {
        return boxTitle;
    }
}
