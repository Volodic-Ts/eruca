package com.example.eruca.helpers;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

//public class for passing data between fragments
public class SharedViewModel extends ViewModel {

    private MutableLiveData<Box> data;

    //use this method when you need to prepare some data for sending from fragment
    public void setBoxValues(Box box) {
        data.setValue(box);
    }

    //use this method when you need to take prepared data in another fragment
    public MutableLiveData<Box> getData() {
        if (data == null) {
            data = new MutableLiveData<>();
        }

        return data;
    }
}
