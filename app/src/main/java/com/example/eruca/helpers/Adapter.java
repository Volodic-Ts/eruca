package com.example.eruca.helpers;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;
import java.util.List;

//public class-adapter to manipulate fragment
public class Adapter extends FragmentStateAdapter {
    private List<Fragment> list = new ArrayList<>();

    public void addFragment(Fragment fragment) {
        list.add(fragment);
    }

    public Adapter(FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return list.get(position);
    }
}