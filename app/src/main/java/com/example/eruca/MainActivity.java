package com.example.eruca;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import com.example.eruca.helpers.Adapter;
import com.example.eruca.helpers.SharedViewModel;
import com.example.eruca.ui.modulesState.ModulesStateFragment;
import com.example.eruca.ui.qr.QrScannerFragment;
import com.example.eruca.ui.sensorsData.SensorsDataFragment;
import com.example.eruca.ui.technicalCard.TechnicalCardFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private SharedViewModel sharedViewModel;

    //init variables for android's data file
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor mEditor;

    //string for auth token
    private String TOKEN;

    //init variables for bottom-nav control
    private BottomNavigationView bottomNavigationView;
    private ViewPager2 viewPager;
    private Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        TOKEN = getTokenFromFile();

        Log.e("MainActivityToken: ", TOKEN);

        viewPager = findViewById(R.id.container);
        bottomNavigationView = findViewById(R.id.nav_view);

        //add bottom-nav fragments to adapter
        adapter = new Adapter(this);
        adapter.addFragment(new SensorsDataFragment());
        adapter.addFragment(new ModulesStateFragment());
        adapter.addFragment(new TechnicalCardFragment());
        adapter.addFragment(new QrScannerFragment());
        viewPager.setAdapter(adapter);

        //show modal dialog with user's boxes after long click on bottom-nav icon
        bottomNavigationView.findViewById(R.id.navigation_sensors_data).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                showModalDialog();

                return true;
            }
        });

        //change fragments in bottom-nav
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_sensors_data:
                    viewPager.setCurrentItem(0, true);
                    break;
                case R.id.navigation_modules_state:
                    viewPager.setCurrentItem(1, true);
                    break;
                case R.id.navigation_tech_card:
                    viewPager.setCurrentItem(2, true);
                    break;
                case R.id.navigation_qr_scanner:
                    viewPager.setCurrentItem(3, true);
                    break;
            }
            return true;
        });

        //synchronize fragments and bottom-nav icons
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        bottomNavigationView.setSelectedItemId(R.id.navigation_sensors_data);
                        break;
                    case 1:
                        bottomNavigationView.setSelectedItemId(R.id.navigation_modules_state);
                        break;
                    case 2:
                        bottomNavigationView.setSelectedItemId(R.id.navigation_tech_card);
                        break;
                    case 3:
                        bottomNavigationView.setSelectedItemId(R.id.navigation_qr_scanner);
                        break;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
        } else {
            finish();
        }
    }

    private void showModalDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        GetBoxesFragment getBoxesFragment = GetBoxesFragment.newInstance("Boxes");
        getBoxesFragment.show(fragmentManager, "fragment get boxes");
    }

    private String getTokenFromFile() {
        sharedPreferences = this.getSharedPreferences("tokenPrefs", Context.MODE_PRIVATE);

        return sharedPreferences.getString("token", "");
    }

    public String getToken() {
        return TOKEN;
    }
}