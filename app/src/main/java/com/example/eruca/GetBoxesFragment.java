package com.example.eruca;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.eruca.helpers.Box;
import com.example.eruca.helpers.SharedViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GetBoxesFragment extends DialogFragment {

    //init view model to passing data to another fragment
    private SharedViewModel sharedViewModel;

    //init queue for get boxes request
    private RequestQueue mQueue;

    //init list to create boxes list in modal window
    private ListView mListBoxes;

    //init global json array for getting boxes
    private JSONArray jsonArray;

    //init token string for auth
    private String TOKEN;

    public GetBoxesFragment() {
    }

    public static GetBoxesFragment newInstance(String title) {
        GetBoxesFragment fragment = new GetBoxesFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_get_boxes, container);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //init queue for get request from server
        mQueue = Volley.newRequestQueue(getContext());

        //get token for auth form parent activity
        MainActivity activity = (MainActivity) getActivity();
        TOKEN = activity.getToken();

        mListBoxes = view.findViewById(R.id.boxes_list);

        jsonParse();

        //prepare box data for passing after clicking on list item
        mListBoxes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String itemValue = (String) mListBoxes.getItemAtPosition(i);

                //get box id and box title from list item
                String id = itemValue.substring(4, 28);
                String name = itemValue.substring(36);

                //pass data in view model
                Box box = new Box(id, name);
                sharedViewModel.setBoxValues(box);

                //close modal window
                dismiss();
            }
        });

        mListBoxes.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);
    }

    void jsonParse() {
        String url = BuildConfig.SERVER_URL + "/api/box";

        ArrayList<String> arrayList = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, arrayList);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            try {
                jsonArray = response.getJSONArray("Boxes");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject boxes = jsonArray.getJSONObject(i);

                    String boxId = boxes.getString("id");
                    String boxName = boxes.getString("qrHash");

                    arrayList.add("ID: " + boxId + "\n" + "Title: " + boxName);

                    mListBoxes.setAdapter(adapter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::printStackTrace) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("Content-Type", "application/json; charset=UTF-8");
                headerMap.put("Authorization", "Bearer " + TOKEN);

                return headerMap;
            }
        };

        mQueue.add(request);
    }
}
